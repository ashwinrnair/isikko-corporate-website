const http = require('http');
const https = require('https');
const fs = require('fs');
const { format } = require('date-fns');
const path = require('path');

// Configuration
const API_URL = 'https://api.isikko.com/api/v1/sitemap';
const SITEMAP_FILE = path.join(__dirname, 'sitemap.xml');

// Fetch data from API
function fetchSitemapData(url) {
    return new Promise((resolve, reject) => {
        https.get(url, (resp) => {
            let data = '';

            resp.on('data', (chunk) => {
                data += chunk;
            });

            resp.on('end', () => {
                try {
                    resolve(JSON.parse(data));
                } catch (e) {
                    reject(e);
                }
            });

        }).on('error', (err) => {
            reject(err);
        });
    });
}

// Create XML Sitemap
function createSitemapXml(data) {
    let xml = '<?xml version="1.0" encoding="UTF-8"?>\n';
    xml += '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">\n';

    data.forEach(item => {
        xml += '  <url>\n';
        xml += `    <loc>${item.loc}</loc>\n`;
        xml += `    <priority>${item.priority || 0.5}</priority>\n`;
        xml += `    <lastmod>${item.lastmod || format(new Date(), 'yyyy-MM-dd')}</lastmod>\n`;
        xml += '  </url>\n';
    });

    xml += '</urlset>\n';
    return xml;
}

// Save XML Sitemap to file
function saveSitemap(filename, xmlContent) {
    fs.writeFile(filename, xmlContent, (err) => {
        if (err) {
            console.error('Error saving sitemap:', err);
        } else {
            console.log(`Sitemap saved to ${filename}`);
        }
    });
}

// Handle HTTP requests
const server = http.createServer(async (req, res) => {
    if (req.url === '/sitemap.xml') {
        try {
            const data = await fetchSitemapData(API_URL);
            const xmlContent = createSitemapXml(data);
            saveSitemap(SITEMAP_FILE, xmlContent);

            res.writeHead(200, { 'Content-Type': 'application/xml' });
            res.end(xmlContent);
        } catch (error) {
            console.error('Error generating sitemap:', error);
            res.writeHead(500, { 'Content-Type': 'text/plain' });
            res.end('Internal Server Error');
        }
    } else {
        res.writeHead(404, { 'Content-Type': 'text/plain' });
        res.end('Not Found');
    }
});

// Start the server
const PORT = process.env.PORT || 8080;
server.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
