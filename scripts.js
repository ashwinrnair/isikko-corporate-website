document.addEventListener("DOMContentLoaded", (event) => {
  if (window.innerWidth > 768) {
    showBookings("br0", "bookings");
  } else {
    showBookings('br0', 'bookings');
    showBookings('am1', 'assets');
    showBookings('pb1', 'payments');
    showBookings('al1', 'analytics');
    showBookings('bb1', 'brandBuilding');
    showBookings('ux1', 'ux');
  }

  window.addEventListener("scroll", () => {
    // const header = document.getElementById("header");
    if (window.scrollY > 3000) {
      //   header.classList.add("scrolled");
    } else {
      //   header.classList.remove("scrolled");
    }
  });

  //scroll functionality
  var delay = false;
  var currentPage = 1;
  var pageCount = document.querySelectorAll(".scroll-item").length;
  var headerHeight = document.querySelector('.header').offsetHeight;
  var headerLinks = document.querySelectorAll('.ulHeader .headerValue');
  var sections = document.querySelectorAll('.scroll-item');
  let headerDiv = document.querySelector('header.header > div');

  function addActiveClass(page) {
    headerLinks.forEach(link => link.classList.remove('active'));
    if (page !== null) {
      var currentLink = document.querySelector(`.ulHeader .headerValue[data-bs-target="#sec${page}"]`);
      if (currentLink) {
        currentLink.classList.add('active');
      }
    }
  }

  function getCurrentPage() {
    let page = null;
    sections.forEach((section, index) => {
      const sectionTop = section.offsetTop - headerHeight;
      const sectionBottom = sectionTop + section.offsetHeight;
      const scrollPosition = window.scrollY + window.innerHeight / 2;

      if (scrollPosition >= sectionTop && scrollPosition < sectionBottom) {
        page = index + 1;
      }
    });
    return page;
  }

  function handleScroll(event) {
    if (delay) return;
    delay = true;
    setTimeout(function () { delay = false }, 100);

    if (currentPage === null) {
      currentPage = getCurrentPage();
    }
    if (currentPage !== null) {
      var wd = event.deltaY;
      var currentSection = document.getElementById('sec' + currentPage);
      var sectionTop = currentSection.offsetTop;
      var sectionBottom = sectionTop + currentSection.offsetHeight;
      var currentPosition = window.scrollY + window.innerHeight / 2;

      if (wd > 0) {
        // Scroll down
        if (currentPosition >= sectionBottom - 100 && currentPage < pageCount) {
          currentPage++;
          var targetSection = document.getElementById('sec' + currentPage);
          var targetPosition = targetSection.offsetTop - headerHeight;

          window.scrollTo({
            top: targetPosition + 30,
            behavior: 'smooth'
          });
        }
      } else {
        // Scroll up
        if (window.scrollY <= sectionTop - window.innerHeight / 2 && currentPage > 1) {
          currentPage--;
        }
      }
      currentPage = getCurrentPage();
    }
  }

  document.addEventListener('wheel', handleScroll, { passive: false });

  //menu button functionality
  const menuToggle = document.querySelector('.menuToggle');
  const ulHeader = document.querySelector('.ulHeader');

  menuToggle.addEventListener('click', function () {
    ulHeader.classList.toggle('show');
  });

  //menu item navigation functionality
  var menuOpen = false;
  var menuIcon = document.querySelector('.menuIcon');

  document.querySelectorAll('.ulHeader a').forEach(anchor => {
    anchor.addEventListener('click', function (e) {
      e.preventDefault();
      ulHeader.classList.toggle('show');
      menuIcon.src = './assets/21.png';
      menuOpen = !menuOpen;

      const targetSelector = this.getAttribute('data-bs-target');
      const targetElement = document.querySelector(targetSelector);

      window.scrollTo({
        top: targetElement.offsetTop - headerHeight + 20,
        behavior: 'smooth'
      });
    });
  });

  //logo nav functionality
  let logo = document.querySelector('.Mainlogo');
  if (logo) {
    logo.addEventListener('click', function () {
      window.scrollTo({
        top: 0,
        behavior: "smooth"
      });
      addActiveClass(1);
    });
  }

  //menu icon change functionality
  menuToggle.addEventListener('click', function () {
    if (menuOpen) {
      menuIcon.src = './assets/21.png';
    } else {
      menuIcon.src = './assets/menuCrossIcon.png';
    }
    menuOpen = !menuOpen;
  });

  //Intersection Observation logic
  var observerOptions = {
    root: null,
    rootMargin: '0px',
    threshold: [0.5, 0.8]
  };

  function handleIntersection(entries, observer) {
    entries.forEach(entry => {
      var header = document.querySelector('.header');

      //header change logic
      if (entry.intersectionRatio >= 0.8) {
        if (entry.target.id === 'panel' || entry.target.id === 'keepHidden') {
          if (entry.isIntersecting) {
            header.classList.add('hidden-header');
          }
        }
        if (entry.target.id === 'start' || entry.target.id === 'end') {
          if (entry.isIntersecting) {
            header.classList.remove('hidden-header');
          }
        }

        // header container fluid change logic
        if (entry.target.id === 'showFluid') {
          if (entry.isIntersecting) {
            headerDiv.classList.add('container-fluid');
            headerDiv.classList.remove('container');
          }
        }
        if (entry.target.id === 'showNotFluid') {
          //execute this if showNotFluid is intersecting or if it is above the current viewport
          if (entry.isIntersecting || (entry.boundingClientRect.top < 0 && entry.boundingClientRect.bottom < 0)) {
            headerDiv.classList.add('container');
            headerDiv.classList.remove('container-fluid');
          }
        }
      }

      //change active menu link
      if (entry.intersectionRatio >= 0.5 && entry.target.id.startsWith('sec')) {
        const intersectionArea = entry.intersectionRect.width * entry.intersectionRect.height;
        const viewportArea = entry.rootBounds.width * entry.rootBounds.height;

        const coverageRatio = intersectionArea / viewportArea;

        // Check if the element covers more than 50% of the viewport
        if (coverageRatio >= 0.5) {
          addActiveClass(entry.target.id.slice(3));
        }
      }
    });
  }

  var observer = new IntersectionObserver(handleIntersection, observerOptions);
  observer.observe(document.querySelector('#start'));
  observer.observe(document.querySelector('#end'));
  observer.observe(document.querySelector('#panel'));
  observer.observe(document.querySelector('#keepHidden'));
  observer.observe(document.querySelector('#showFluid'));
  observer.observe(document.querySelector('#showNotFluid'));
});

function showBookings(subsection, section) {
  if (window.innerWidth > 768) {
    hideAllSections();
  } else {
    hideAllSections(section);
  }
  var sectionel = document.getElementById(section);
  sectionel.style.display = "block";
  var subsectionel = document.getElementById(subsection);
  subsectionel.style.display = "flex";
  var linkel = document.getElementById("link-" + subsection);
  linkel.classList.add("selectedSubHeading");
  var headingel = document.getElementById("heading-" + section);
  headingel.classList.add("activeClass");
}

function hideAllSections(section = null) {
  var sections = ["bookings", "payments", "brandBuilding", "ux", "assets", "analytics"];
  var bookingsections = ["0", "1", "2", "3", "4", "5", "6"];
  var paymentsections = ["1", "2", "3", "4", "5", "6"];
  var brandbuildingsections = ["1", "2", "3", "4", "5", "6"];
  var uxsections = ["1", "2", "3", "4"];
  var assetsections = ["1", "2", "3", "4", "5", "6", "7"];
  var analyticssections = ["1", "2", "3", "4"];
  if (!section) {

    for (var i = 0; i < sections.length; i++) {
      var el = document.getElementById(sections[i]);
      el.style.display = "none";
      var linkel = document.getElementById("heading-" + sections[i]);
      if (linkel) linkel.classList.remove("activeClass");
    }

    for (var i = 0; i < bookingsections.length; i++) {
      var el = document.getElementById("br" + bookingsections[i]);
      el.style.display = "none";
      var linkel = document.getElementById("link-br" + bookingsections[i]);
      if (linkel) linkel.classList.remove("selectedSubHeading");
    }
    for (var i = 0; i < paymentsections.length; i++) {
      var el = document.getElementById("pb" + paymentsections[i]);
      el.style.display = "none";
      var linkel = document.getElementById("link-pb" + paymentsections[i]);
      if (linkel) linkel.classList.remove("selectedSubHeading");
    }
    for (var i = 0; i < brandbuildingsections.length; i++) {
      var el = document.getElementById("bb" + brandbuildingsections[i]);
      el.style.display = "none";
      var linkel = document.getElementById("link-bb" + brandbuildingsections[i]);
      if (linkel) linkel.classList.remove("selectedSubHeading");
    }
    for (var i = 0; i < uxsections.length; i++) {
      var el = document.getElementById("ux" + uxsections[i]);
      el.style.display = "none";
      var linkel = document.getElementById("link-ux" + uxsections[i]);
      if (linkel) linkel.classList.remove("selectedSubHeading");
    }
    for (var i = 0; i < assetsections.length; i++) {
      var el = document.getElementById("am" + assetsections[i]);
      el.style.display = "none";
      var linkel = document.getElementById("link-am" + assetsections[i]);
      if (linkel) linkel.classList.remove("selectedSubHeading");
    }
    for (var i = 0; i < analyticssections.length; i++) {
      var el = document.getElementById("al" + analyticssections[i]);
      el.style.display = "none";
      var linkel = document.getElementById("link-al" + analyticssections[i]);
      if (linkel) linkel.classList.remove("selectedSubHeading");
    }
  } else if (sections.includes(section)) {
    // Hide only the specified section
    sectionsMapping = {
      bookings: bookingsections,
      payments: paymentsections,
      brandBuilding: brandbuildingsections,
      ux: uxsections,
      assets: assetsections,
      analytics: analyticssections
    }
    sectionsShortMapping = {
      bookings: "br",
      payments: "pb",
      brandBuilding: "bb",
      ux: "ux",
      assets: "am",
      analytics: "al"
    }

    var subSections = sectionsMapping[section];
    for (var i = 0; i < subSections.length; i++) {
      var el = document.getElementById(sectionsShortMapping[section] + subSections[i]);
      if (el) el.style.display = "none";
      var linkel = document.getElementById("link-" + sectionsShortMapping[section] + subSections[i]);
      if (linkel) linkel.classList.remove("selectedSubHeading");
    }
  }
}

window.addEventListener('load', () => {
  const validPaths = ['/', '/privacy-policy', '/terms-of-service', '/cookie-policy', '/faqs', '/why-isikko', '/contact-us', '/solutions','/sitemap.xml'];
  const currentPath = window.location.pathname;

  if (!validPaths.includes(currentPath)) {
    fetch('./404.html')
      .then(response => response.text())
      .then(html => {
        document.documentElement.innerHTML = html;
      })
      .catch(err => {
        console.error('Error loading 404 page:', err);
      });
  }
});
